package main

import (
    "bufio"
    "fmt"
//    "io"
//    "io/ioutil"
    "os"
    "strconv"
)

func main() {
    var numbers []int

    file, _ := os.Open("input.txt")

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        num, _ := strconv.ParseInt(scanner.Text(), 10, 32)
        numbers = append(numbers, int(num))
    }

    for i := 0; i < len(numbers); i++ {
        for j := 0; j < len(numbers); j++ {
            for k := 0; k < len(numbers); k++ {
                if i != j && i != k && j != k {
                    if numbers[i] + numbers[j] + numbers[k] == 2020 {
                        fmt.Println(numbers[i] * numbers[j] * numbers[k])
                    }
                }
            }
        }
    }
}
