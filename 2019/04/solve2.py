import math
count = 0
for num in range(178416, 676461+1):
    adjacent = 0 # good
    adjCount = 0 # int
    decrease = 0 # bad
    for divisor in [1, 10, 100, 1000, 10000]: #, 100000]:
        digit = math.floor(num/divisor) % 10
        nextDigit = math.floor(num/(divisor*10)) % 10
        if nextDigit > digit:
            decrease = 1
        if digit == nextDigit:
            adjCount = adjCount + 1
        else:
            if adjCount == 1:
                adjacent = 1
            adjCount = 0
    if adjCount == 1:
        adjacent = 1

    if adjacent and not decrease:
    #    print(num)
        count = count + 1
print(count)
# 1439 too high
