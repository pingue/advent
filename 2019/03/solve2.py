from grid import Position


position=Position(0,0)
boards = [{Position(0,0): 0}, {Position(0,0): 0}]
board = 0
with open('input.txt') as file:
    for line in file:
        position=Position(0,0)
        length=1
        for instruction in line.split(','):
            for i in range(0, int(instruction[1:])):
                if instruction[0] == "R":
                    position = Position(position.x+1, position.y)
                if instruction[0] == "L":
                    position = Position(position.x-1, position.y)
                if instruction[0] == "U":
                    position = Position(position.x, position.y+1)
                if instruction[0] == "D":
                    position = Position(position.x, position.y-1)
                if position not in boards[board]:
                    boards[board][position] = length
#                print (position)
#                print(board[position])
#                print(board)
#                boards[board][position] = boards[board][position] + 1
                length = length +1
        board = board+1

#print(board)
for j in range(20, -1, -1):
    for i in range(0,20):
        p = Position(i,j)
        char = " "
        if (p in boards[0]):
            char = boards[0][p]%10
        if (p in boards[1]):
            char = boards[1][p]%10
        if (p in boards[0]) and (p in boards[1]):
            char = "X"
        print(char, end='')
    print("")

closestDistance = 1000000
closestPoint = Position(1000,1000)
for p in boards[0].keys():
    if p in boards[1] and p != Position(0,0):
        distance = boards[0][p] + boards[1][p]
        if distance < closestDistance:
            closestDistance = distance
            closestPoint = p

print (closestDistance)
print(closestPoint)

# 439 too low
# 441 too low
