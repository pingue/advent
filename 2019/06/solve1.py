orbits = {}

def getDepth(item):
    if item == "COM":
        return 0
    return 1 + getDepth(orbits[item])

with open('input.txt') as file:
    for line in file:
        parent, child = line.strip().split(")")
        orbits[child] = parent

count = 0
for item in orbits.keys():
    count += getDepth(item)

print(count)
