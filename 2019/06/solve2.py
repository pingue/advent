orbits = {}

def getDescendants(item):
    if item == "COM":
        return ["COM"]
    descs = getDescendants(orbits[item])
    descs.append(item)
    return descs

def getDepth(item, target):
    if item == target:
        return 0
    return 1 + getDepth(orbits[item], target)

with open('input.txt') as file:
    for line in file:
        parent, child = line.strip().split(")")
        orbits[child] = parent

myDescs = getDescendants("YOU")
sanDescs = getDescendants("SAN")
commonDescs = [value for value in myDescs if value in sanDescs]

print(getDepth("YOU", commonDescs[-1]) - 2 + getDepth("SAN", commonDescs[-1]))
