import math

file = open('input.txt')
totalFuel = 0
for line in file:
    print("Input line: " + line)
    fuel = math.floor(int(line)/3)-2
    print("Fuel: " + str(fuel))
    totalFuel += fuel
    print("Cumulative fuel: " + str(totalFuel))
    print("---")
print
print("Total fuel: " + str(totalFuel))
