import math

def calculateFuel(weight):
    if weight < 9: # Precalculated to save a math call
        return 0
    fuelWeight = math.floor(weight/3)-2
    return fuelWeight + calculateFuel(fuelWeight)

file = open('input.txt')
totalFuel = 0
for line in file:
    print("Input line: " + line)
    moduleFuel = calculateFuel(int(line))
    print("Fuel: " + str(moduleFuel))
    totalFuel += moduleFuel
    print("Cumulative fuel: " + str(totalFuel))
    print("---")
print
print("Total fuel: " + str(totalFuel))

