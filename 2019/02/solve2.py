import sys
file = open('input.txt')

fileline = file.readline().strip()
isetbase = []

for i in fileline.split(','):
    isetbase.append(int(i))

def solve(iset):
    ins = 0
#    print(len(iset))
    while True:
#        print(ins)
        if iset[ins] == 99:
 #           print("END")
            break
        elif iset[ins] == 1:
  #          print("ADD" + str(iset[ins+1]) + " " + str(iset[ins+2])+ " ST " + str(iset[ins+3]))
            num1 = iset[iset[ins+1]]
            num2 = iset[iset[ins+2]]
            total = num1 + num2
            iset[iset[ins+3]] = total
        elif iset[ins] == 2:
            #print("MUL" + str(iset[ins+1]) + " " + str(iset[ins+2])+ " ST " + str(iset[ins+3]))
            num1 = iset[iset[ins+1]]
            num2 = iset[iset[ins+2]]
            total = num1 * num2
            iset[iset[ins+3]] = total
        #print (iset)
        ins += 4
    return iset[0]

for noun in range(0, 99):
    for verb in range(0,99):
        isetbase[1] = noun
        isetbase[2] = verb
        print("N "+  str(noun) + " V " + str(verb))
        solution = solve(isetbase.copy())
        print(solution)
        if solution == 19690720:
            print(str((100*verb)+ noun))
            sys.exit(0)

# 986 too low
