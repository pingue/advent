import math

file = open('input.txt')

fileline = file.readline().strip()
iset = []

for i in fileline.split(','):
    iset.append(int(i))
#iset[1] = 12
#iset[2] = 2
##print(iset)

ins = 0

while True:
#    #print(ins)
    operation = iset[ins]
    oplen = 0
    opcode = operation % 100
    p1mode = math.floor(operation/100) % 10
    p2mode = math.floor(operation/1000) % 10
    p3mode = math.floor(operation/10000) % 10
    if opcode == 99: # END
        #print("END")
        break
    elif opcode == 1: # ADD
        #print("ADD")
        oplen = 4
        par1 = iset[ins+1]
        par2 = iset[ins+2]
        par3 = iset[ins+3]

        if p1mode:
            num1 = par1
        else:
            num1 = iset[par1]
        if p2mode:
            num2 = par2
        else:
            num2 = iset[par2]

        total = num1 + num2
        iset[par3] = total
    elif opcode == 2: # MUL
        #print("MUL")
        oplen = 4
        par1 = iset[ins+1]
        par2 = iset[ins+2]
        par3 = iset[ins+3]

        if p1mode:
            num1 = par1
        else:
            num1 = iset[par1]
        if p2mode:
            num2 = par2
        else:
            num2 = iset[par2]

        total = num1 * num2
        iset[par3] = total
    elif opcode == 3: # INPUT
        oplen = 2
        val = input("INPUT>")
        par1 = iset[ins+1]
        iset[par1] = int(val)
    elif opcode == 4: # OUTPUT
        #print("OUT")
        oplen = 2
        par1 = iset[ins+1]
        print(iset[par1])
    elif opcode == 5: # JUMP-IF-TRUE
        #print("JIT")
        par1 = iset[ins+1]
        par2 = iset[ins+2]
        oplen = 3
        if p1mode:
            num1 = par1
        else:
            num1 = iset[par1]
        if p2mode:
            num2 = par2
        else:
            num2 = iset[par2]
        if num1:
            oplen = 0
            ins = num2
    elif opcode == 6: # JUMP-IF-FALSE
        par1 = iset[ins+1]
        par2 = iset[ins+2]
        oplen = 3
        if p1mode:
            num1 = par1
        else:
            num1 = iset[par1]
        if p2mode:
            num2 = par2
        else:
            num2 = iset[par2]
        #print("JIF" + str(par1) + str(par2)+ str(num1)+str(num2))
        if not num1:
            oplen = 0
            ins = num2
    elif opcode == 7: # LT
        #print("LT")
        par1 = iset[ins+1]
        par2 = iset[ins+2]
        par3 = iset[ins+3]
        if p1mode:
            num1 = par1
        else:
            num1 = iset[par1]
        if p2mode:
            num2 = par2
        else:
            num2 = iset[par2]
        oplen = 4
        iset[par3] = int(num1<num2)
    elif opcode == 8: # EQ
        #print("EQ")
        par1 = iset[ins+1]
        par2 = iset[ins+2]
        par3 = iset[ins+3]
        if p1mode:
            num1 = par1
        else:
            num1 = iset[par1]
        if p2mode:
            num2 = par2
        else:
            num2 = iset[par2]
        oplen = 4
        iset[par3] = int(num1==num2)


#    #print (iset)
    ins += oplen
##print(iset[0])

