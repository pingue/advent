INPUT=37
ITERATIONS="157901"

recipeList = str(INPUT)

elf1position = 0
elf2position = 1

while True:
    newSum = int(recipeList[elf1position]) + int(recipeList[elf2position])
    recipeList = recipeList + str(newSum)
    elf1position = (elf1position + int(recipeList[elf1position]) + 1) % len(recipeList)
    elf2position = (elf2position + int(recipeList[elf2position]) + 1) % len(recipeList)
    if ITERATIONS in recipeList:
        print len(recipeList) - len(ITERATIONS)
        break



