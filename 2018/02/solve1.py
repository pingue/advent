from file import *
from  string import *
fname="input.txt"

count2s = 0
count3s = 0
lines = get_file_as_array_of_lines(fname)

for line in lines:
    counts = letters_count(line)
    if 2 in counts.values():
        count2s = count2s + 1

    if 3 in counts.values():
        count3s = count3s + 1


print (count3s * count2s)
