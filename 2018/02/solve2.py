from file import *
from string import *

fname="input.txt"

lines = get_file_as_array_of_lines(fname)


def find_first_matching_box():
    for line1 in lines:
        for line2 in lines:
            if different_letter_count_by_position(line1, line2) == 1:
                return find_differing_letters_by_position(line1, line2)



print find_first_matching_box()
