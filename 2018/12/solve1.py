import sys
from file import *
fname="input.txt"
GENERATIONS = 20

ruleInput = get_file_as_array_of_lines(fname)

pots = {}
for i in range(-50, 150): # Slightly more than the list can grow to
    pots[i] = 0
rules = {}

i = 0
lowpot = 0
for pot in ruleInput[0].strip("initial state: "):
    if pot == "#":
        pots[i] = 1
    else:
        pots[i] = 0
    i += 1
highpot = i

for rule in ruleInput[2:]:
    ruleBinary = rule.split(" ")[0].replace("#", "1").replace(".", "0")
    ruleOutput = int(rule.split("> ")[1] == "#")
    rules[ruleBinary] = ruleOutput

def printpots(potVar):
    for key in sorted(potVar):
        sys.stdout.write(str(potVar[key]))
    print 

for generation in range(GENERATIONS):
    newPots = {}
    lowpot -= 2
    highpot += 2
    for pot in range(lowpot, highpot):
        matchstring = ""
        for i in range(pot-2, pot+3):
            matchstring = matchstring + str(pots[i])
        if matchstring in rules: # For test input only
            newPots[pot] = rules[matchstring]
        else:
            newPots[pot] = 0
    for newPot in newPots:
        pots[newPot] = newPots[newPot]

generationsum = 0
for pot in pots:
    if pots[pot]:
        generationsum += pot

print generationsum

#wrong: 3503
