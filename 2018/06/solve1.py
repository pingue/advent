import sys
from file import *
from dict import *
from grid import *
from array import *
fname="input.txt"

coords = get_file_as_array_of_lines(fname)
positions = []
maxWidth = 0
maxHeight = 0
for coord in coords:
    pos = Position(coord.split(",")[0].strip(" "), coord.split(",")[1].strip(" "))
    positions.append(pos)
    if pos.x > maxWidth:
        maxWidth = pos.x
    if pos.y > maxHeight:
        maxHeight = pos.y

grid = {}
closeCount = {}
infiniteList = set()
for x in range(maxWidth + 1):
    for y in range(maxHeight + 1):
        mypos = Position(x, y)
        closestPos = None
        closest = sys.maxint
        for pos in positions:
            if pos.calculate_manhatten_distance(mypos) == closest:
                closestPos = None
                continue
            if pos.calculate_manhatten_distance(mypos) < closest:
                closestPos = pos
                closest = pos.calculate_manhatten_distance(mypos)
        if x == 0 or y == 0 or x == maxWidth or y == maxHeight:
            infiniteList.add(closestPos)
        grid[mypos] = closestPos
        if closestPos in closeCount:
            closeCount[closestPos] = closeCount[closestPos] + 1
        else:
            closeCount[closestPos] = 1
        if closestPos in infiniteList:
            closeCount[closestPos] = 0


print closeCount[dict_max_val(closeCount)]

