import sys
from file import *
from dict import *
from grid import *
from array import *
fname="input.txt"

coords = get_file_as_array_of_lines(fname)
positions = []
maxWidth = 0
maxHeight = 0
for coord in coords:
    pos = Position(coord.split(",")[0].strip(" "), coord.split(",")[1].strip(" "))
    positions.append(pos)
    if pos.x > maxWidth:
        maxWidth = pos.x
    if pos.y > maxHeight:
        maxHeight = pos.y

regionCount = 0
for x in range(maxWidth + 1):
    for y in range(maxHeight + 1):
        mypos = Position(x, y)
        manhattanSum = 0
        for pos in positions:
            manhattanSum = manhattanSum + pos.calculate_manhatten_distance(mypos)
        if manhattanSum < 10000:
            regionCount = regionCount + 1

print regionCount

