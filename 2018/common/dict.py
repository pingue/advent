def dict_max_val(dic):
    inverse = [(value, key) for key, value in dic.items()]
    return max(inverse)[1]

