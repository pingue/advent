def letters_count(string):
    # Count the number of each letter in the string
    counts = {}
    for letter in string:
        if letter in counts:
            counts[letter] = counts[letter] + 1
        else:
            counts[letter] = 1

    return counts

def different_letter_count_by_position(string1, string2):
    # Check each index of the string and calculate how many positions differ between the two
    # NB - abcde and edcba would return 4
    countDiff = 0
    for i in range(len(string1)):
        if string1[i] != string2[i]:
            countDiff = countDiff + 1

    return countDiff

def find_differing_letters_by_position(string1, string2):
    # Check each index of the string and return a string of the characters which are the same in 
    # both input strings
    # NB - abcde and edcba would only return c
    matching_chars = ""
    for i in range(len(string1)):
        if string1[i] == string2[i]:
            matching_chars = matching_chars + string1[i]

    return matching_chars

def alphabet_to_number(letter):
    return ord(letter.lower()) - 96
