def get_file_as_array_of_lines(fname):
    return [line.rstrip('\n') for line in open(fname)]

def get_file_as_array_of_ints(fname):
    lines = get_file_as_array_of_lines(fname)
    ints = []
    for line in lines:
        if line.strip() != "":
            if line.startswith("+"):
                ints.append(int(line.strip()[1:]))
            else:
                ints.append(int(line.strip()))
    return ints
