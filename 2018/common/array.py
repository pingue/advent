def mode(in_array):
    element_count = {}
    for element in in_array:
        if element in element_count:
            element_count[element] = element_count[element] + 1
        else:
            element_count[element] = 1

    inverse = [(value, key) for key, value in element_count.items()]
    return max(inverse)[1]
