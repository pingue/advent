class Velocity:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __repr__(self):
        return "VEL[" + str(self.x) + "," + str(self.y) + "]"
    def __str__(self):
        return "V[" + str(self.x) + "," + str(self.y) + "]"
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.x == other.x and self.y == other.y
        return False
    def __ne__(self, other):
        return not self.__eq__(other)
    def __hash__(self):
        return hash((self.x, self.y))

class Position:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def __repr__(self):
        return "POS[" + str(self.x) + "," + str(self.y) + "]"
    def __str__(self):
        return "P[" + str(self.x) + "," + str(self.y) + "]"
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.x == other.x and self.y == other.y
        return False
    def __ne__(self, other):
        return not self.__eq__(other)
    def __hash__(self):
        return hash((self.x, self.y))
    def __lt__(self, other):
        if self.y == other.y:
            return self.x < other.x
        return self.y < other.y

    def calculate_manhatten_distance(self, other):
        xdiff = abs(self.x - other.x)
        ydiff = abs(self.y - other.y)
        return xdiff + ydiff


def convert_position_and_size_to_array_of_pos(left, top, width, height):
    array_of_coords = []
    for x in range(int(width)):
        for y in range(int(height)):
            array_of_coords.append(Position(int(left) + x, int(top) + y))
    return array_of_coords
