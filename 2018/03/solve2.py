from file import *
from string import *
from grid import *
fname="input.txt"

lines = get_file_as_array_of_lines(fname)

position_count = {}

def check_uniqueness_of_claim(posArray):
    for pos in posArray:
        if position_count[pos] != 1:
            return False
    return True

for line in lines:
    left = line.split(" ")[2].split(",")[0]
    top = line.split(" ")[2].split(",")[1].strip(":")
    width = line.split(" ")[3].split("x")[0]
    height = line.split(" ")[3].split("x")[1]

    posArray = convert_position_and_size_to_array_of_pos(left, top, width, height)

    for pos in posArray:
        if pos not in position_count:
            position_count[pos] = 1
        else:
            position_count[pos] = position_count[pos] + 1

for line in lines:
    left = line.split(" ")[2].split(",")[0]
    top = line.split(" ")[2].split(",")[1].strip(":")
    width = line.split(" ")[3].split("x")[0]
    height = line.split(" ")[3].split("x")[1]

    posArray = convert_position_and_size_to_array_of_pos(left, top, width, height)
    if check_uniqueness_of_claim(posArray):
        print line.split(" ")[0].strip("#")
        break

