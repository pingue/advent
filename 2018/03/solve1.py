from file import *
from string import *
from grid import *
fname="input.txt"

lines = get_file_as_array_of_lines(fname)

position_count = {}

for line in lines:
    left = line.split(" ")[2].split(",")[0]
    top = line.split(" ")[2].split(",")[1].strip(":")
    width = line.split(" ")[3].split("x")[0]
    height = line.split(" ")[3].split("x")[1]

    posArray = convert_position_and_size_to_array_of_pos(left, top, width, height)

    for pos in posArray:
        if pos not in position_count:
            position_count[pos] = 1
        else:
            position_count[pos] = position_count[pos] + 1

print len(dict((k, v) for k, v in position_count.items() if v >= 2))
