import sys
from grid import Position
from file import *
fname="input.txt"

lines = get_file_as_array_of_lines(fname)

ground = set()
water = set()
WATERSTART=Position(500,0)
water.add(WATERSTART)

minheight = 999
height = 0
yposes = set()
xposes = set()
currentPositions = [WATERSTART]
for line in lines:
    x = line.split("x=")[1].split(", ")[0]
    y = line.split("y=")[1].split(", ")[0]
    if ".." in x:
        x = range(int(x.split("..")[0]), int(x.split("..")[1]) + 1)
    else:
        x = [int(x)]
    if ".." in y:
        y = range(int(y.split("..")[0]), int(y.split("..")[1]) + 1)
    else:
        y = [int(y)]

    for xpos in x:
        xposes.add(xpos)
        for ypos in y:
            yposes.add(ypos)
            ground.add(Position(xpos, ypos))
            if ypos < minheight:
                minheight = ypos
            if ypos > height:
                height = ypos

def fillLineWithWater(position):
    x = position.x
    y = position.y
    response = set()
    waterAdd = set()
    global water
    global newCurrentPositions

    while True:
        x -= 1

        if Position(x, y) in ground:
            break
        if Position(x, y+1) not in ground and Position(x, y+1) not in water:
            response.add(Position(x, y))
            waterAdd.add(Position(x, y))
            break
        if Position(x, y) in water and Position(x-1, y+1) not in ground and Position(x-1, y+1) not in water:
            break
        waterAdd.add(Position(x, y))

    x = position.x
    while True:
        x += 1
        if Position(x, y) in ground:
            break
        if Position(x, y+1) not in ground and Position(x, y+1) not in water:
            response.add(Position(x, y))
            waterAdd.add(Position(x, y))
            break
        if Position(x, y) in water and Position(x+1, y+1) not in ground and Position(x+1, y+1) not in water:
            break
        waterAdd.add(Position(x, y))
    if len(response) == 0:
        response.add(Position(position.x, y - 1))
    print "B"
    print newCurrentPositions
    newCurrentPositions = newCurrentPositions.difference(waterAdd)
    print "C"
    print newCurrentPositions
    water = water.union(waterAdd)
    print "F"
    print response
    return response

def isLineBoundedByGround(position):
    x = position.x
    y = position.y
    leftBound = False
    rightBound = False
    groundBeneath = True
    while Position(x, y+1) in ground or Position(x, y+1) in water:
        x += 1
        groundBeneath = Position(x-1, y+1) in ground
        if Position(x, y) in ground:
            rightBound = True

    x = position.x
    while Position(x, y+1) in ground or Position(x, y+1) in water:
        x -= 1
        groundBeneath = Position(x+1, y+1) in ground
        if Position(x, y) in ground:
            leftBound = True

    return groundBeneath or (leftBound or rightBound)

def drawGround(minx, maxx):
    for y in range(minx, maxx):
    #for y in range(314, 387):
        sys.stdout.write(str(y).ljust(4))
        for x in range(min(xposes), max(xposes) + 1):
            if Position(x, y) in water:
                sys.stdout.write(str(x%10))
            else:
                if Position(x, y) in ground:
                    sys.stdout.write(u"\u2588")
                else:
                    if x == 500:
                        sys.stdout.write(u"\u2591")
                    else:
                        sys.stdout.write(" ")
        print 

i = 0
while True:
    i += 1
    newCurrentPositions = set()
    for currentPosition in currentPositions:
        nextEvaluate = Position(currentPosition.x, currentPosition.y + 1)
        if currentPosition.y >= height:
            continue
        if nextEvaluate not in ground and nextEvaluate not in water:
            water.add(nextEvaluate)
            newCurrentPositions.add(nextEvaluate)
            print "D"
            print newCurrentPositions
        else:
            if isLineBoundedByGround(currentPosition):
                newCurrentPositions = newCurrentPositions.union(fillLineWithWater(currentPosition))
            print "E"
            print currentPositions
            print newCurrentPositions

    if len(newCurrentPositions) == 0:
        break
    currentPositions = newCurrentPositions
    if 200<i<300:
        print "A"
        print currentPositions
        drawGround(50, 134)
drawGround(0, max(yposes) + 1)

heightedWater = set()
for wat in water:
    if wat.y >= minheight:
        heightedWater.add(wat)
    else:
        print wat
count = 0
sys.stdout.write("    ")
for x in range(min(xposes), max(xposes) + 1):
    sys.stdout.write(str(int(x/10)%10))
print



print Position(500,0) in water

print len(heightedWater)
# Wrong: 308 (too low)
# Wrong: 309 (too low)
# Wrong: 124202 (too high)
