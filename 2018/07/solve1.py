from file import *
from dict import *
fname="input.txt"

deplines = get_file_as_array_of_lines(fname)

nodeArray = set()
dependencies = {} # {node: [requires1, requires2]}
for dep in deplines:
    node = dep.split(" ")[7]
    req = dep.split(" ")[1]
    nodeArray.add(node)
    nodeArray.add(req)
    if node in dependencies:
        dependencies[node].append(req)
    else:
        dependencies[node] = [req]


outputOrder = ""

#for dep in dependencies.keys().sort():
#    if dep not in 

while (dependencies):
    for node in sorted(nodeArray):
        if node not in dependencies:
            outputOrder = outputOrder + node
            for d in nodeArray:
                if d in dependencies and node in dependencies[d]:
                    dependencies[d].remove(node)
                if d in dependencies and dependencies[d] == []:
                    del dependencies[d]
            nodeArray.remove(node)
            break
outputOrder = outputOrder + nodeArray.pop()
print outputOrder


