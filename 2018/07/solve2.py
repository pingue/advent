from file import *
from string import *
from dict import *
fname="input.txt"
WORKERS = 5
DELAY = 60

deplines = get_file_as_array_of_lines(fname)

nodeArray = set()
dependencies = {} # {node: [requires1, requires2]}
for dep in deplines:
    node = dep.split(" ")[7]
    req = dep.split(" ")[1]
    nodeArray.add(node)
    nodeArray.add(req)
    if node in dependencies:
        dependencies[node].append(req)
    else:
        dependencies[node] = [req]


completedTimes = {}
workerFreeAt = {}
pendingNodes = set(nodeArray)

def find_next_job(sec):
    for node in sorted(pendingNodes):
        skipNode = False 
        if node in dependencies:
            for dependent in dependencies[node]:
                if dependent in completedTimes:
                    if completedTimes[dependent] >= sec:
                        skipNode = True
                else:
                    skipNode = True


        if not skipNode:
            pendingNodes.remove(node)
            return node

    return None


for i in range(WORKERS):
    workerFreeAt[i] = -1

sec = 0
while pendingNodes:
    for worker in range(WORKERS):
        if sec > workerFreeAt[worker]:
            job = find_next_job(sec)
            if job:
                completiontime = sec + DELAY + alphabet_to_number(job) - 1
                workerFreeAt[worker] = completiontime
                completedTimes[job] = completiontime
    sec = sec + 1

print max(completedTimes.values()) + 1


# Wrong: 29, 82
