from file import *
from array import *
import sys
fname="input.txt"

guardlog = sorted(get_file_as_array_of_lines(fname))
guard = -1
asleep = -1

guardAsleepMinutes = {}

for entry in guardlog:
    minute = int(entry.split(" ")[1].split(":")[1].strip("]"))

    if entry.split(" ")[2] == "wakes":
        if guard in guardAsleepMinutes:
            guardAsleepMinutes[guard].extend(range(asleep, minute))
        else:
            guardAsleepMinutes[guard] = range(asleep, minute)
        asleep = -1
    elif entry.split(" ")[2] == "falls":
        asleep = minute
    elif entry.split(" ")[2] == "Guard":
        guard = int(entry.split(" ")[3].strip("#"))
    else:
        print "ERROR WITH " + entry
        sys.exit(1)


sleepiestGuard = (-1, -1, -1)
for guard in guardAsleepMinutes:
    timeAsleep = len(guardAsleepMinutes[guard])
    if timeAsleep > sleepiestGuard[1]:
        sleepiestGuard = (guard, timeAsleep, guardAsleepMinutes[guard])

print sleepiestGuard[0] * mode(sleepiestGuard[2])
