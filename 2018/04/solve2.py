from file import *
from array import *
from collections import Counter
import sys
fname="input.txt"

guardlog = sorted(get_file_as_array_of_lines(fname))
guard = -1
asleep = -1

guardAsleepMinutes = {}

for entry in guardlog:
    minute = int(entry.split(" ")[1].split(":")[1].strip("]"))

    if entry.split(" ")[2] == "wakes":
        if guard in guardAsleepMinutes:
            guardAsleepMinutes[guard].extend(range(asleep, minute))
        else:
            guardAsleepMinutes[guard] = range(asleep, minute)
        asleep = -1
    elif entry.split(" ")[2] == "falls":
        asleep = minute
    elif entry.split(" ")[2] == "Guard":
        guard = int(entry.split(" ")[3].strip("#"))
    else:
        print "ERROR WITH " + entry
        sys.exit(1)


sleepiestMinute = (-1, -1, -1) # guard, minute, frequency
for guard in guardAsleepMinutes:
    sleepiestGuardMinute = Counter(guardAsleepMinutes[guard]).most_common(1)

    if sleepiestGuardMinute[0][1] > sleepiestMinute[2]:
        sleepiestMinute = (guard, sleepiestGuardMinute[0][0], sleepiestGuardMinute[0][1])

print sleepiestMinute[0] * sleepiestMinute[1]
