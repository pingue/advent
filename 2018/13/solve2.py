import time
import sys
from file import *
from grid import Position
fname="input.txt"

filemap = get_file_as_array_of_lines(fname)

cartmap = {}
x = y = 0
for line in filemap:
    x = 0
    for char in line:
        pos = Position(x, y)
        if char in ["v", "^"]:
            char = (char, "|", 0) # Next direction, underme, junctioncount
        if char in [">", "<"]:
            char = (char, "-", 0) # Next direction, underme, junctioncount
        cartmap[pos] = char
        x += 1
    y += 1

height = max((pos.y for pos in cartmap.keys())) + 1
width = max((pos.x for pos in cartmap.keys())) + 1

def printmap():
    print 
    print
#    print('\033c')
    for y in range(height):
        for x in range(width):
            outchar = cartmap[Position(x, y)]
            if isinstance(outchar, tuple):
                outchar = outchar[0]
            sys.stdout.write(outchar)
        print

tick = 0
while True:
    tick += 1
    newMap = {}
    if tick >= 160:
        printmap()
    if tick > 170:
        sys.exit(1)
    for y in range(height):
        for x in range(width):
            currentPos = cartmap[Position(x, y)]
            if isinstance(currentPos, tuple):
                if currentPos[0] == ">":
                    newPos = Position(x + 1, y)
                if currentPos[0] == "<":
                    newPos = Position(x - 1, y)
                if currentPos[0] == "v":
                    newPos = Position(x, y + 1)
                if currentPos[0] == "^":
                    newPos = Position(x, y - 1)
                
                newDirection = currentPos[0]
                if cartmap[newPos] == "+":
                    if currentPos[0] == "^":
                        if currentPos[2] % 3 == 0:
                            newDirection = "<"
                        if currentPos[2] % 3 == 2:
                            newDirection = ">"
                    if currentPos[0] == ">":
                        if currentPos[2] % 3 == 0:
                            newDirection = "^"
                        if currentPos[2] % 3 == 2:
                            newDirection = "v"
                    if currentPos[0] == "v":
                        if currentPos[2] % 3 == 0:
                            newDirection = ">"
                        if currentPos[2] % 3 == 2:
                            newDirection = "<"
                    if currentPos[0] == "<":
                        if currentPos[2] % 3 == 0:
                            newDirection = "v"
                        if currentPos[2] % 3 == 2:
                            newDirection = "^"
                    currentPos = (currentPos[0], currentPos[1], currentPos[2] + 1)
                if cartmap[newPos] in ["\\", "/"]:
                    if currentPos[0] == "^" and cartmap[newPos] == "/":
                        newDirection = ">"
                    if currentPos[0] == "v" and cartmap[newPos] == "/":
                        newDirection = "<"
                    if currentPos[0] == "<" and cartmap[newPos] == "/":
                        newDirection = "v"
                    if currentPos[0] == ">" and cartmap[newPos] == "/":
                        newDirection = "^"
                    if currentPos[0] == "^" and cartmap[newPos] == "\\":
                        newDirection = "<"
                    if currentPos[0] == "v" and cartmap[newPos] == "\\":
                        newDirection = ">"
                    if currentPos[0] == "<" and cartmap[newPos] == "\\":
                        newDirection = "^"
                    if currentPos[0] == ">" and cartmap[newPos] == "\\":
                        newDirection = "v"

                if newPos in newMap and isinstance(newMap[newPos], tuple) or (
                        isinstance(cartmap[newPos], tuple) and Position(x, y) in newMap and
                        isinstance(newMap[Position(x, y)], tuple)):
                    print newPos
                    if isinstance(cartmap[newPos], tuple):
                        newMap[newPos] = cartmap[newPos][1]
                        cartmap[newPos] = cartmap[newPos][1]
                    else:
                        newMap[newPos] = cartmap[newPos]
                    newMap[Position(x, y)] = currentPos[1]
                else:
                    newMap[newPos] = (newDirection, cartmap[newPos], currentPos[2])
                    newMap[Position(x, y)] = currentPos[1]
            else:
                if Position(x, y) not in newMap:
                    newMap[Position(x, y)] = currentPos
    cartmap = newMap
    print str(tick) + ":" + str(len(list((i for i in cartmap.values() if isinstance(i, tuple)))))
    print sorted(list((i for i in cartmap.items() if isinstance(i[1], tuple))))
    if len(list((i for i in cartmap.values() if isinstance(i, tuple)))) == 1:
        print [key for key, value in cartmap.items() if isinstance(value, tuple)][0]
        sys.exit(0)

