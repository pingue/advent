import time
import sys
from file import *
from grid import *
fname="input.txt"

sky = []
sky_render = []
lights = get_file_as_array_of_lines(fname)
for light in lights:
    light_x = int(light.split("<")[1].split(",")[0])
    light_y = int(light.split(",")[1].split(">")[0])
    light_xv = int(light.split("<")[2].split(",")[0])
    light_yv = int(light.split(",")[2].split(">")[0])
    sky.append((Position(light_x, light_y), Velocity(light_xv, light_yv)))

i = 0
while True:
    i = i + 1
    min_x = 0
    min_y = 0
    max_x = 0
    max_y = 0
    new_sky = []
    for light in sky:
        new_light = Position(light[0].x + light[1].x, light[0].y + light[1].y)
        new_sky.append((new_light, light[1]))
        if new_light.x > max_x:
            max_x = new_light.x
        if new_light.y > max_y:
            max_y = new_light.y
        if new_light.x < min_x:
            min_x = new_light.x
        if new_light.y < min_y:
            min_y = new_light.y
    sky = new_sky
    if max_x - min_x < 200 and max_y - min_y < 200:
        skylights = [x for x,_ in sky]
        for y in range(min_y-1, max_y+1):
            sys.stdout.flush()
            for x in range(min_x-1, max_x+1):
                if Position(x, y) in skylights:
                    sys.stdout.write("#")
                else:
                    sys.stdout.write(" ")
        print
        time.sleep(1)
        print i
