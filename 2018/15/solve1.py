import random
import time
import sys
from file import *
from grid import *
fname="input.txt"

lines = get_file_as_array_of_lines(fname)

class Player:
    def __init__(self, team, pos):
        self.team = team
        self.health = 200 # random.randint(1, 200)
        self.position = pos

    def takeDamage(self):
        self.health -= 3

    def otherTeam(self):
        if self.team == "G":
            return "E"
        else:
            return "G"

    def __repr__(self):
        return self.team + "[" + str(self.position.x) + "," + str(self.position.y) + ", <3: " + str(self.health) + "]"
    def __str__(self):
        return self.team + "[" + str(self.position.x) + "," + str(self.position.y) + ", <3: " + str(self.health) + "]"
    def __lt__(self, other):
        return self.position < other.position
game = {}

goblins = []
elves = []

y = 0
for line in lines:
    x = 0
    for char in line:
        game[Position(x, y)] = char
        if char == "E":
            elves.append(Player(char, Position(x, y)))
        if char == "G":
            goblins.append(Player(char, Position(x, y)))
        x += 1
    y += 1

def drawBoard():
    sys.stderr.write("\x1b[2J\x1b[H")
    for y in range(max(game.keys()).y + 1):
        for x in range(max(game.keys()).x + 1):
            sys.stdout.write(game[Position(x,y)])
        print

def getAdjacentSquares(pos):
    return [Position(pos.x - 1, pos.y), Position(pos.x + 1, pos.y), Position(pos.x, pos.y - 1), Position(pos.x, pos.y + 1)]

def getLowestHealth(enemyList):
    lowestHealth = 201
    lowEnemies = []
    for enemy in enemyList:
        if enemy.health < lowestHealth:
            lowEnemies = [enemy]
            lowestHealth = enemy.health
        if enemy.health == lowestHealth:
            lowEnemies.append(enemy)

    return lowEnemies

drawBoard()

players = goblins+elves
roundNo = 0
while True:
    for player in sorted(players):
        #print player
        #print players
        #print "."
        enemies = list(filter(lambda x: x.team == player.otherTeam(), players))

        squaresInRange = []
        for enemy in enemies:
            for square in getAdjacentSquares(enemy.position):
                if game[square] == "." or game[square] == player.team:
                    squaresInRange.append(square)

        if len(squaresInRange) == 0:
            continue

        if player.position in squaresInRange:
# Attack
            enemyToAttack = None
            enemyList = []
            for square in getAdjacentSquares(player.position):
                if game[square] == player.otherTeam():
                    enemy = [en for en in (goblins+elves) if en.position == square][0]
                    enemyList.append(enemy)
            enemyToAttack = sorted(getLowestHealth(enemyList))[0]
            if enemyToAttack.health <= 3:
                #print "PARSING DEATH"
                #print player
                #print enemyToAttack
                #print players

                players.remove(enemyToAttack)
                #print "DEATH"
            else:
                enemyToAttack.takeDamage()

        else:
# Move
            expandingRing = set()
            expandingRing.add(player.position)
            possibleTargets = []
            start = player.position
            while not possibleTargets:
                newExpandingRing = set(expandingRing)
                for square in expandingRing:
                    for nextSquare in getAdjacentSquares(square):
                        if game[nextSquare] == player.otherTeam():
                            possibleTargets.append(square)
                        else:
                            if game[nextSquare] == ".":
                                newExpandingRing.add(nextSquare)

                if len(expandingRing) == len(newExpandingRing):
                    break
                expandingRing = newExpandingRing
            if not possibleTargets:
                continue
            target = sorted(possibleTargets)[0]

            expandingRing = set()
            expandingRing.add(target)
            possibleMoves = []
            while not possibleMoves:
                newExpandingRing = set(expandingRing)
                for square in expandingRing:
                    for nextSquare in getAdjacentSquares(square):
                        if nextSquare == player.position:
                            possibleMoves.append(square)
                        else:
                            if game[nextSquare] == ".":
                                newExpandingRing.add(nextSquare)
                expandingRing = newExpandingRing
            move = sorted(possibleMoves)[0]
            
            game[player.position] = "."
            player.position = move
            game[player.position] = player.team 
        if len(list(filter(lambda x: x.team == "E", players))) == 0 or len(list(filter(lambda x: x.team == "G", players))) == 0:
            #print roundNo
            #print players
            sys.exit(0)

        drawBoard()
    roundNo += 1
