import time
from grid import *

GRIDSERIAL = 7139

grid = {}

for x in range(1,301):
    for y in range(1,301):
        pos = Position(x, y)
        RackId = x + 10
        PowerLevel = RackId * y
        PowerLevel = PowerLevel + GRIDSERIAL
        PowerLevel = PowerLevel * RackId
        PowerLevel = (PowerLevel / 100) % 10
        PowerLevel = PowerLevel - 5
        grid[pos] = PowerLevel


bestVal = (121, Position(235,66), 9) # val, pos, size
for size in reversed(range(1, 20)): # Should be 301, but taking an educating guess that medium sized is better
    if bestVal[0] > (5 * size * size):
        break
    for x in range(1, 301 - size):
        for y in range(1, 301 - size):
            threeVal = 0
            for i in range(size):
                for j in range(size):
                    threePos = Position(x + i, y + j)
                    threeVal = threeVal + grid[threePos]
            if threeVal > bestVal[0]:
                bestVal = (threeVal, Position(x, y), size)

print str(bestVal[1].x) + "," + str(bestVal[1].y) + "," + str(bestVal[2])


