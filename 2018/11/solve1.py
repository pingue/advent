from grid import *

GRIDSERIAL = 7139

grid = {}

for x in range(1,301):
    for y in range(1,301):
        pos = Position(x, y)
        RackId = x + 10
        PowerLevel = RackId * y
        PowerLevel = PowerLevel + GRIDSERIAL
        PowerLevel = PowerLevel * RackId
        PowerLevel = (PowerLevel / 100) % 10
        PowerLevel = PowerLevel - 5
        grid[pos] = PowerLevel


bestVal = (0, 0)
for x in range(1, 298):
    for y in range(1, 298):
        threeVal = 0
        for i in range(3):
            for j in range(3):
                threePos = Position(x + i, y + j)
                threeVal = threeVal + grid[threePos]
        if threeVal > bestVal[0]:
            bestVal = (threeVal, Position(x, y))

print str(bestVal[1].x) + "," + str(bestVal[1].y)


