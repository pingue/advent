from file import *
fname="inputa.txt"

lines = get_file_as_array_of_lines(fname)
samples = []

for line in range(len(lines)/4):
    before = map(int, lines[line*4].replace("Before: [", "").replace("]", "").split(", "))
    op = map(int, lines[line*4 + 1].split(" "))
    after = map(int, lines[line*4 + 2].replace("After:  [", "").replace("]", "").split(", "))
    samples.append((before, op, after))

def addr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] + out[op[2]]
    return out
def addi(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] + op[2]
    return out
def mulr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] * out[op[2]]
    return out
def muli(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] * op[2]
    return out
def banr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] & out[op[2]]
    return out
def bani(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] & op[2]
    return out
def borr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] | out[op[2]]
    return out
def bori(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] | op[2]
    return out
def setr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]]
    return out
def seti(op, inp):
    out = list(inp)
    out[op[3]] = op[1]
    return out
def gtir(op, inp):
    out = list(inp)
    out[op[3]] = int(op[1] > out[op[2]])
    return out
def gtri(op, inp):
    out = list(inp)
    out[op[3]] = int(out[op[1]] > op[2])
    return out
def gtrr(op, inp):
    out = list(inp)
    out[op[3]] = int(out[op[1]] > out[op[2]])
    return out
def eqir(op, inp):
    out = list(inp)
    out[op[3]] = int(op[1] == out[op[2]])
    return out
def eqri(op, inp):
    out = list(inp)
    out[op[3]] = int(out[op[1]] == op[2])
    return out
def eqrr(op, inp):
    out = list(inp)
    out[op[3]] = int(out[op[1]] == out[op[2]])
    return out


possibleOps = []
for i in range(16):
    possibleOps.append(["addr", "addi", "mulr", "muli", "banr", "bani", "borr", "bori", "setr", "seti", "gtir", "gtri", "gtrr", "eqir", "eqri", "eqrr"])

for sample in samples:
    before = sample[0]
    op = sample[1]
    after = sample[2]
    
    if not int(addr(op, before) == after) and "addr" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("addr")
    if not int(addi(op, before) == after) and "addi" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("addi")
    if not int(mulr(op, before) == after) and "mulr" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("mulr")
    if not int(muli(op, before) == after) and "muli" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("muli")
    if not int(banr(op, before) == after) and "banr" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("banr")
    if not int(bani(op, before) == after) and "bani" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("bani")
    if not int(borr(op, before) == after) and "borr" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("borr")
    if not int(bori(op, before) == after) and "bori" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("bori")
    if not int(setr(op, before) == after) and "setr" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("setr")
    if not int(seti(op, before) == after) and "seti" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("seti")
    if not int(gtir(op, before) == after) and "gtir" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("gtir")
    if not int(gtri(op, before) == after) and "gtri" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("gtri")
    if not int(gtrr(op, before) == after) and "gtrr" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("gtrr")
    if not int(eqir(op, before) == after) and "eqir" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("eqir")
    if not int(eqri(op, before) == after) and "eqri" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("eqri")
    if not int(eqrr(op, before) == after) and "eqrr" in possibleOps[op[0]]:
        possibleOps[op[0]].remove("eqrr")
#    print possibleOps[op[0]]

#print possibleOps
#0 eqir
#1 borr
#2 addr
#3 gtri
#4 muli
#5 gtir
#6 mulr
#7 banr
#8 bori
#9 eqri
#10 eqrr
#11 bani
#12 setr
#13 gtrr
#14 addi
#15 seti

fname="inputb.txt"

programlines = get_file_as_array_of_lines(fname)

state = [0,0,0,0] #map(int, programlines[0].split(" "))

for programline in programlines:#[1:]
    programline = map(int, programline.split(" "))
    if programline[0] == 0:
        print "eqir" + str(programline)
        state = eqir(programline, state)
    if programline[0] == 1:
        print "borr" + str(programline)
        state = borr(programline, state)
    if programline[0] == 2:
        print "addr" + str(programline)
        state = addr(programline, state)
    if programline[0] == 3:
        print "gtri" + str(programline)
        state = gtri(programline, state)
    if programline[0] == 4:
        print "muli" + str(programline)
        state = muli(programline, state)
    if programline[0] == 5:
        print "gtir" + str(programline)
        state = gtir(programline, state)
    if programline[0] == 6:
        print "mulr" + str(programline)
        state = mulr(programline, state)
    if programline[0] == 7:
        print "banr" + str(programline)
        state = banr(programline, state)
    if programline[0] == 8:
        print "bori" + str(programline)
        state = bori(programline, state)
    if programline[0] == 9:
        print "eqri" + str(programline)
        state = eqri(programline, state)
    if programline[0] == 10:
        print "eqrr" + str(programline)
        state = eqrr(programline, state)
    if programline[0] == 11:
        print "bani" + str(programline)
        state = bani(programline, state)
    if programline[0] == 12:
        print "setr" + str(programline)
        state = setr(programline, state)
    if programline[0] == 13:
        print "gtrr" + str(programline)
        state = gtrr(programline, state)
    if programline[0] == 14:
        print "addi" + str(programline)
        state = addi(programline, state)
    if programline[0] == 15:
        print "seti" + str(programline)
        state = seti(programline, state)
    print state

print state
# Wrong: 15
# Wrong: 657 (Too Low)
