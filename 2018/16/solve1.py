from file import *
fname="inputa.txt"

lines = get_file_as_array_of_lines(fname)
samples = []

for line in range(len(lines)/4):
    before = map(int, lines[line*4].replace("Before: [", "").replace("]", "").split(", "))
    op = map(int, lines[line*4 + 1].split(" "))
    after = map(int, lines[line*4 + 2].replace("After:  [", "").replace("]", "").split(", "))
    samples.append((before, op, after))

def addr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] + out[op[2]]
    return out
def addi(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] + op[2]
    return out
def mulr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] * out[op[2]]
    return out
def muli(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] * op[2]
    return out
def banr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] & out[op[2]]
    return out
def bani(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] & op[2]
    return out
def borr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] | out[op[2]]
    return out
def bori(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] | op[2]
    return out
def setr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]]
    return out
def seti(op, inp):
    out = list(inp)
    out[op[3]] = op[1]
    return out
def gtir(op, inp):
    out = list(inp)
    out[op[3]] = op[1] > out[op[2]]
    return out
def gtri(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] > op[2]
    return out
def gtrr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] > out[op[2]]
    return out
def eqir(op, inp):
    out = list(inp)
    out[op[3]] = op[1] == out[op[2]]
    return out
def eqri(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] == op[2]
    return out
def eqrr(op, inp):
    out = list(inp)
    out[op[3]] = out[op[1]] == out[op[2]]
    return out

goodcount = 0
for sample in samples:
    before = sample[0]
    op = sample[1]
    after = sample[2]
    
    matches = 0
    matches += int(addr(op, before) == after)
    matches += int(addi(op, before) == after)
    matches += int(mulr(op, before) == after)
    matches += int(muli(op, before) == after)
    matches += int(banr(op, before) == after)
    matches += int(bani(op, before) == after)
    matches += int(borr(op, before) == after)
    matches += int(bori(op, before) == after)
    matches += int(setr(op, before) == after)
    matches += int(seti(op, before) == after)
    matches += int(gtir(op, before) == after)
    matches += int(gtri(op, before) == after)
    matches += int(gtrr(op, before) == after)
    matches += int(eqir(op, before) == after)
    matches += int(eqri(op, before) == after)
    matches += int(eqrr(op, before) == after)
    if matches >= 3:
        goodcount += 1
print goodcount
