from file import *
fname="input.txt"

input = get_file_as_array_of_lines(fname)

flat = [int(x) for x in input[0].split(" ")]

def parseChild(remainingString):
    output = []
    childCount = remainingString[0]
    metaDataCount = remainingString[1]
    remainingString = remainingString[2:]
    for child in range(childCount):
        childOutput = parseChild(remainingString)
        output.append(childOutput[0])
        remainingString = childOutput[1]
    for metaData in range(metaDataCount):
        output.append(remainingString[0])
        remainingString = remainingString[1:]
    return (output, remainingString)

# MOVE TO MODULE
def flatten(container):
    for i in container:
        if isinstance(i, (list,tuple)):
            for j in flatten(i):
                yield j
        else:
            yield i

tree = parseChild(flat)[0]


print sum(list(flatten(tree)))
