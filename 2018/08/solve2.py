from file import *
fname="input.txt"

input = get_file_as_array_of_lines(fname)

flat = [int(x) for x in input[0].split(" ")]

def parseChild(remainingString):
    value = 0
    inputstring = remainingString
    childCount = remainingString[0]
    childArray = []
    metaDataCount = remainingString[1]
    metaDataArray = []
    remainingString = remainingString[2:]
    for child in range(childCount):
        childOutput = parseChild(remainingString)
        childArray.append(childOutput[0])
        remainingString = childOutput[1]
    for metaData in range(metaDataCount):
        metaDataArray.append(remainingString[0])
        remainingString = remainingString[1:]
    if childCount == 0:
        value = sum(metaDataArray)
    else:
        for metaData in metaDataArray:
            if len(childArray) >= metaData:
                value = value + childArray[metaData - 1]
    return (value, remainingString)

val = parseChild(flat)[0]
print val

#wrong: 0
