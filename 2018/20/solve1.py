import re
from file import *
from grid import *
fname="input8.txt"

lines = get_file_as_array_of_lines(fname)
currentPos = Position(0, 0)

routeArray = []


#def parseRoute(route):
#    print route
#    maxRoute = route.split("(")[0]
#    if "(" in route:
#        maxRoute += parseRoute(route.split("(", 1)[1].rsplit(")", 1)[0])
#    maxRoute += route.split(")")[-1]
#    return maxRoute

route = lines[0].strip("^").strip("$")

#print parseRoute(route)
while route.find("(") != -1:
    searchObj = re.search(r'(.*)\(([NEWS]*)\|([NEWS]*)\)(.*)', route)
    try:
        if len(searchObj.group(2)) == 0 or len(searchObj.group(3)) == 0:
            route = searchObj.group(1) + searchObj.group(4)
        else:
            route = searchObj.group(1) + max([searchObj.group(2), searchObj.group(3)], key=len) + searchObj.group(4)
    except:
        searchObj = re.search(r'(.*)\(([NEWS]*)\|([NEWS]*)\|([NEWS]*)\)(.*)', route)
        if len(searchObj.group(2)) == 0 or len(searchObj.group(3)) == 0 or len(searchObj.group(4)) == 0:
            route = searchObj.group(1) + searchObj.group(5)
        else:
            route = searchObj.group(1) + max([searchObj.group(2), searchObj.group(3), searchObj.group(4)], key=len) + searchObj.group(5)


print route
print len(route)

# Wrong: 3312 (too low)
