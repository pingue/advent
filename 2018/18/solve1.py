import sys
from file import *
from grid import *
fname="input.txt"

ITERATIONS = 10

lines = get_file_as_array_of_lines(fname)

forest = {}

y = 0
for line in lines:
    x = 0
    for acre in line:
        forest[Position(x, y)] = acre
        x += 1
    y += 1

width = x
height = y


def forestPrint():
    for line in range(height):
        for acre in range(width):
            sys.stdout.write(forest[Position(acre, line)])
        print

def getSurroundingAcres(position):
    surroundingAcres = []
    for y in range(position.y - 1, position.y + 2):
        for x in range(position.x - 1, position.x + 2):
            if Position(x, y) in forest and not (y == position.y and x == position.x):
                surroundingAcres.append(forest[Position(x, y)])
    return surroundingAcres

#forestPrint()
for i in range(ITERATIONS):
    newForest = {}
    for y in range(height):
        for x in range(width):
            pos = Position(x, y)
            character = forest[pos]
            surround = getSurroundingAcres(pos)
            if character == ".":
                if surround.count("|") >= 3:
                    newForest[pos] = "|"
                else:
                    newForest[pos] = forest[pos]
            if character == "|":
                if surround.count("#") >= 3:
                    newForest[pos] = "#"
                else:
                    newForest[pos] = forest[pos]
            if character == "#":
                if surround.count("#") >= 1 and surround.count("|") >= 1:
                    newForest[pos] = "#"
                else:
                    newForest[pos] = "."

    forest = newForest
#    forestPrint()
#    print 

print forest.values().count("#") * forest.values().count("|")
    
            

