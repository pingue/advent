NUMBEROFPLAYERS = 462
NUMBEROFMARBLES = 71938

boardState = [0]
currentElf = 1
lastLocation = 0
elfscores = {} # 1 indexed elves

for marble in range(NUMBEROFMARBLES):
    currentMarble = marble + 1
    if currentElf > NUMBEROFPLAYERS:
        currentElf = 1

    if currentMarble % 23 == 0:
        scoreToAdd = currentMarble + boardState[(lastLocation - 7) % len(boardState)]
        newLastLocation = (lastLocation - 7) % len(boardState)
        del boardState[(lastLocation - 7) % len(boardState)]
        lastLocation = newLastLocation
        if currentElf in elfscores:
            elfscores[currentElf] = elfscores[currentElf] + scoreToAdd
        else:
            elfscores[currentElf] = scoreToAdd
    else:
        newLocation = (lastLocation + 2)
        if newLocation > len(boardState):
            newLocation = newLocation - len(boardState)
        boardState.insert(newLocation, currentMarble)
        lastLocation = newLocation
    currentElf = currentElf + 1

print max(elfscores.values())
