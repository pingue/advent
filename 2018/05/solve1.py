from file import *
fname="input.txt"

outpoly = ""

def performLoop(polymer):
    skip = 0
    outpoly = ""
    for i in range(len(polymer)):
        if skip == 1:
            skip = 0
            continue
        if polymer[i].islower():
            oppositeCase = polymer[i].upper()
        else:
            oppositeCase = polymer[i].lower()

        if (i < len(polymer) - 1) and (oppositeCase == polymer[i+1]):
            skip = 1
        else:
            outpoly = outpoly + polymer[i]

    return outpoly

polymer = get_file_as_array_of_lines(fname)[0]
changemade = 1
while changemade == 1:
    newpolymer = performLoop(polymer)
    if newpolymer == polymer:
        changemade = 0
    polymer = newpolymer

print len(polymer)
