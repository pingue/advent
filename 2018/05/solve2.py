import string
from file import *
fname="input.txt"

def performLoop(polymer):
    skip = 0
    outpoly = ""
    for i in range(len(polymer)):
        if skip == 1:
            skip = 0
            continue
        if polymer[i].islower():
            oppositeCase = polymer[i].upper()
        else:
            oppositeCase = polymer[i].lower()

        if (i < len(polymer) - 1) and (oppositeCase == polymer[i+1]):
            skip = 1
        else:
            outpoly = outpoly + polymer[i]

    return outpoly

startingpolymer = get_file_as_array_of_lines(fname)[0]
best_blockers = {}

for char in string.ascii_lowercase:
    print char
    polymer = startingpolymer.replace(char, '').replace(char.upper(), '')
    changemade = 1
    while changemade == 1:
        newpolymer = performLoop(polymer)
        if newpolymer == polymer:
            changemade = 0
        polymer = newpolymer
    best_blockers[char] = len(polymer)

inverse = [(value, key) for key, value in best_blockers.items()]
print min(inverse)[0]
